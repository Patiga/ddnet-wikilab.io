#!/usr/bin/env python3
from PIL import Image
import numpy as np

entities = "entities_clear.png"
game = "entities.png"
front = "front.png"
tele = "tele.png"
speedup = "speedup.png"
switch = "switch.png"
tune = "tune.png"
paths = [entities, game, front, tele, speedup, switch, tune]

def main():
    # parses tiles.csv
    # creates base dictionary for all tiles, maps each unique tile to a unique index
    with open("tiles.csv") as f:
        mapping = {}
        desc = None
        for line in f:
            line = [string.strip() for string in line.split(";")]
            index = int(line[0])
            label = line[1]
            if line[2]:
                desc = line[2]
            if index == 140: # Credits Tile
                dim = (4, 2)
            elif index == 190: # Entities Off Warning Tile
                dim = (2, 1)
            else:
                dim = (1, 1)
            if len(line) == 4 and line[3] != "":
                link = line[3]
            else:
                link = None
            mapping[index] = [label, desc, dim, [], link]
    
    # parses pngs of layers and maps all significant indices to its visual representation in that layer
    layers = [parselayer(path, mapping) for path in paths]

    # removes blank/empty tiles (except with index 0)
    layers = [cleanselayer(layer, mapping) for layer in layers]

    # maps tile labels + descriptions to their visual representation(s)
    for i, layer in enumerate(layers):
        print("Extracting from {}:".format(paths[i]))
        mapping, count, new = extracttiles(layer, mapping)
        print("    -> extracted {} tiles".format(count))
        print("    {}".format(new))

    mapping[8][3] += [mapping[22][3].pop()] # fix for relocated 'Timed Switch Activator'

    print("Extracting Done!\n")
    print("Mapping Layers...")
    # assigns the tiles of each layer to the correct tile in the base dictionary
    layermaps = [maplayer(mapping, layer) for layer in layers]
    print("   -> Done!")
    
    # parses groups.csv
    groups = [parseranges(line) for line in open("groups.csv")]
    for i in range(len(groups)):
        stop = False
        for i, group1 in enumerate(groups):
            for j, group2 in enumerate(groups[i+1:]):
                if group1 & group2:
                    groups[i] = group1 | group2
                    del(groups[i + 1 + j])
                    stop = True
                    break
            if stop:
                break
    
    # converts groups into the index-set of each layer, makes those accessible via the indexmap
    for layermap in layermaps:
        reversed = {layermap[i][0]:i for i in layermap}
        for group in groups:
            mapped = [reversed[index] for index in group if index in reversed]
            for i in mapped:
                layermap[i][1] = mapped
    for i in range(len(paths)):
        savemappingsvg(layermaps[i], "/explain/{}".format(paths[i]), mapping, "{}.svg".format(paths[i][:-4]))
    #savetiles(mapping)

# converts matrix to a png, opens it on execution
def show(matrix):
    Image.fromarray(matrix.astype("uint8")).show()

# converts matrix to a png, saves it to the given path
def save(matrix, path):
    Image.fromarray(matrix.astype("uint8")).save(path)

# puts all blocks of a layer into a single png, save it to the given path
def savelayer(layer, path):
    tiles = [layer[tile] for tile in layer if layer[tile].shape[1] == 64] # ignores the credit tile for the sake of simplicity
    con = np.concatenate(tiles, axis=1)
    Image.fromarray(con.astype("uint8")).save(path)

# puts all mapped tiles in a horizontal row, columns include all variants of the block. last 'block' is only visible if there are 2 variants, is the subtracted version
def savetiles(mapping):
    print("Tiles in total: {}".format(sum(len(mapping[i][3]) for i in mapping)))
    width = sum(mapping[i][2][0] * 64 for i in mapping)
    height = max(len(mapping[i][3]) * mapping[i][2][1] * 64 for i in mapping)
    img = np.zeros((height+64, width, 4)).astype("int32")
    x = 0
    for i in mapping:
        dim = mapping[i][2]
        y = 0
        visuals = mapping[i][3]
        for j in visuals:
            img[y:y + dim[1]*64, x:x + dim[0]*64] = j
            y += dim[1] * 64
        if len(visuals) == 2:
            img[img.shape[0]-64:img.shape[0], x:x + dim[0]*64] = np.absolute(visuals[0] - visuals[1])
        x += dim[0] * 64
    save(img, "entire.png")

def indextopos(index, dim):
    x = index % 16
    x1 = x * 64
    x2 = (x + dim[0]) * 64
    y = index // 16
    y1 = y * 64
    y2 = (y + dim[1]) * 64
    return (x1, x2, y1, y2)


# takes a matrix that represents a layer, copies tile specified via index
def gettile(matrix, index, mapping):
    if index in mapping:
        dim = mapping[index][2]
    else:
        dim = (1, 1)
    x1, x2, y1, y2 = indextopos(index, dim)
    return matrix[y1:y2, x1:x2]

# maps all parts of a tile-layer that could include a relevant tile
def parselayer(path, mapping):
    matrix = np.array(Image.open(path)).astype("int32")
    istransparent = matrix[:, :, 3:4] == 0
    matrix = np.choose(istransparent, (matrix, 0))
    tiles = {}
    for index in mapping:
        tile = gettile(matrix, index, mapping)
        tiles[index] = tile
    return tiles

# compares matrices of two tiles, tries to figure out if they represent the same tile, while being quite generous
def sametile(m1, m2, debug = False):
    if m1.shape != m2.shape:
        return False
    diff = np.absolute(m1 - m2)
    colordiff = diff[:, :, :3]
    alphadiff = diff[:, :, 3]
    if alphadiff.max() > 200:
        if debug:
            print("ALPHA", alphadiff.max())
        return False
    if colordiff.max() > 30:
        if debug:
            print(np.where(colordiff > 20))
            print("COLOR", colordiff.max())
        return False
    if debug:
        print(colordiff.max(), alphadiff.max())
    return True

# removes all blank/empty tiles from a layer
def cleanselayer(layer, mapping):
    matrix1 = np.array(Image.open(paths[0])).astype("int32")
    matrix2 = np.array(Image.open(paths[1])).astype("int32")
    blanktiles = [gettile(matrix1, i, mapping) for i in [0, 255]]
    blanktiles += [gettile(matrix2, i, mapping) for i in [255]]
    return {i:layer[i] for i in layer if i == 0 or not (any([sametile(layer[i], blank) for blank in blanktiles]) or layer[i].max() == 0)}

# tries to extract unique tile representations from a given layer, compares with already discovered representations in 'mapping' to avoid duplicates
def extracttiles(layer, mapping):
    c = 0
    new = []
    for i in layer:
        duplicate = False
        for tile in mapping[i][3]:
            if sametile(layer[i], tile):
                duplicate = True
                break
        if duplicate:
            continue
        mapping[i][3] += [layer[i]]
        c += 1
        new += [i]
    return mapping, c, new

# maps indices of tiles in given layer to tile indices in the base dictionary
def maplayer(mapping, layer):
    pointers = {}
    for i in layer:
        pointer = None
        tile = layer[i]
        for old in mapping[i][3]:
            if sametile(tile, old):
                pointer = i
                break
        if pointer is None:
            for j in mapping:
                visuals = mapping[j][3]
                for old in visuals:
                    if sametile(tile, old):
                        pointer = j
                        break
                if pointer is not None:
                    break
        if pointer is None:
            raise ValueError("Tile not found in mapping.")
        pointers[i] = [pointer, []]
    return pointers

# parses lines from 'groups.csv'
def parseranges(line):
    indices = []
    for string in line.split(","):
        if "-" in string:
            string = [int(i) for i in string.split("-")]
            indices += list(range(string[0], string[1]+1))
        else:
            indices += [int(string)]
    return set(indices)

def tilesvg(x, y, width, height, index, title, description, isgroup, link = None):
    tabs = 1
    if isgroup:
        tabs += 1
    if link is not None:
        tabs += 1
    formatstring = "    " * tabs + "<rect x=\"{}\" y=\"{}\" width=\"{}\" height=\"{}\" class=\"tooltip\" title=\"<strong>{} ({})</strong><br>{}\"></rect>\n"
    text = formatstring.format(x, y, width, height, title, index, description)
    if link is not None:
        text = ((tabs - 1) * "    " + "<a xlink:href=\"{}\">\n{}"+ (tabs - 1) * "    " + "</a>\n").format(link, text)
    return text


def savemappingsvg(layermap, layerpath, mapping, path):
    template = open("template.svg", "r").read()
    tiles = {}
    for i in layermap:
        tile = mapping[layermap[i][0]]
        dim = tile[2]
        pos = indextopos(i, dim)
        x, y = pos[0], pos[2]
        width = dim[0] * 64
        height = dim[1] * 64
        title = tile[0]
        description = tile[1]
        link = tile[4]
        isgroup = bool(layermap[i][1])
        tiles[i] = tilesvg(x, y, width, height, i, title, description, isgroup, link,)
    text = ""
    indices = [i for i in layermap]
    while indices:
        i = indices[0]
        if layermap[i][1]:
            text += "    <g>\n"
            for j in layermap[i][1]:
                text += tiles[j]
                indices.remove(j)
            text += "    </g>\n"
        else:
            text += tiles[i]
            del(indices[0])
    with open(path, "w") as w:
        w.write(template.format(image_file = layerpath, explanations = text))
if __name__ == "__main__":
    main()
